# moviebot

Watch free movies, tv shows and documentaries on YouTube, Google Drive, Vimeo and Dailymotion.

## How to Use

1. Register a new application at [twitter](https://dev.twitter.com)
2. Sign up for an API key at [TMDb](https://www.themoviedb.org)
2. Update config.py.
3. Run ```pip install -r requirements.txt```
4. Run ```python moviebot.py``` to retreive new submissions and post to twitter. I've set the bot to run every hour.

## View on Twitter

https://twitter.com/FreeMovieBot
