# moviebot

A Twitter bot that posts free movies, TV shows and documentaries found on YouTube, Google Drive, Vimeo and Dailymotion.

## How to Use

1. Register a new application at [Twitter](https://dev.twitter.com)
2. Sign up for an API key at [TMDb](https://www.themoviedb.org)
2. Update config.py with your Twitter and TMDb API keys.
3. Run ```pip install -r requirements.txt```
4. Run ```python moviebot.py```

## View on Twitter

https://twitter.com/FreeMovieBot
