CREATE DATABASE moviebot;
USE moviebot;

CREATE TABLE `movies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tmdb_id` int(11) NOT NULL,
  `title` varchar(60) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `poster_path` varchar(60) NOT NULL,
  `url` varchar(120) NOT NULL DEFAULT '',
  `deleted` tinyint(4) NOT NULL,
  `popularity` double NOT NULL,
  `vote_average` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
