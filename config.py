# Twitter application settings
consumer_key = ''
consumer_secret = ''
access_key = ''
access_secret = ''

# TMDb API key
tmdb_api_key = ''

# Pictures temp directory
pictures_path = '~/'

# TMDb base image path
tmdb_base_image_path = 'http://image.tmdb.org/t/p/w500'

# Movie resources
resources = [
    'fullmoviesonyoutube',
    'fullmoviesonvimeo',
    'documentaries',
    'fullmoviesdailymotion',
    'FullTVshowsonYouTube',
    'StreamingEpisodes',
    'fulltvshowsonvimeo',
    'fullforeignmovies',
    'biographyfilms'
]
