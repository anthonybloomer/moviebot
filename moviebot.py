# -*- coding: utf-8 -*-

import sys
import os
import praw
import config
import tweepy
import schedule
import time
import urllib
from os.path import expanduser
from tmdbv3api import TMDb

r = praw.Reddit(user_agent='Full movies and documentaries on YouTube, Google Drive, Vimeo and Dailymotion.')

auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
auth.set_access_token(config.access_key, config.access_secret)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

pictures_path = expanduser(config.pictures_path)


def post(submissions, resource):
    print('Searching for new submissions at ' + resource)
    lines = open('data.txt', 'r+')
    ids = lines.read().split()
    for submission in submissions:
        if submission.id not in ids:           
            lines.write(submission.id + '\n')
            title = submission.title.split('(')[0]
            if len(title) > 0:
                tmdb = TMDb(api_key=config.tmdb_api_key)
                try:
                    search = tmdb.search(title)
                except:
                    continue
                if search:
                    try:
                        first = search[0]
                        movie = tmdb.get_movie(first.id)
                        try:
                            genre = '#%s' % movie.genres[0]['name']
                        except IndexError:
                            genre = ''
                        image = first.poster_path
                        if image:
                            image_path = pictures_path + image
                            urllib.urlretrieve(config.tmdb_base_image_path + image, image_path)
                            status = '%s - %s #movies #films %s' % (first.title, submission.url, genre.lower())
                            api.update_with_media(image_path, status=status)
                            os.remove(image_path)
                            print('Status updated: ' + status)
                    except ValueError:
                        continue


def bot():
    for resource in config.resources:
        submissions = r.get_subreddit(resource).get_top_from_hour()
        post(submissions=submissions, resource=resource)


def main():
    bot()
    schedule.every(60).minutes.do(bot)
    while 1:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit()
